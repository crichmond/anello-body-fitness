'use strict';

module.exports = function( grunt ) {

  grunt.initConfig( {
    pkg: grunt.file.readJSON( 'package.json' ),
    sass: {
      local: {
        options: {
          outputStyle: 'nested',
          sourceMap: true,
          includePaths: [
            'bower_components/bourbon/app/assets/stylesheets',
            'scss'
          ]
        },
        files: [ {
          expand: true, // Enable dynamic expansion.
          cwd: 'scss/', // Src matches are relative to this path.
          src: [ '**/*.scss' ], // Actual pattern(s) to match.
          dest: 'dist/css/', // Destination path prefix.
          ext: '.css' // Dest filepaths will have this extension.
        } ]
      },
      deploy: {
        options: {
          outputStyle: 'compressed',
          sourceMap: false,
          includePaths: [
            'bower_components/bourbon/app/assets/stylesheets',
            'scss'
          ]
        },
        files: [ {
          expand: true, // Enable dynamic expansion.
          cwd: 'scss/', // Src matches are relative to this path.
          src: [ '**/*.scss' ], // Actual pattern(s) to match.
          dest: 'dist/css/', // Destination path prefix.
          ext: '.css' // Dest filepaths will have this extension.
        } ]
      }
    },

    uglify: {
      options: {
        preserveComments: false,
        screwIE8: false
      },
      main: {
        files: [ {
          expand: true,
          cwd: "dist/js",
          src: "*.js",
          dest: "dist/js"
        } ]
      }
    },


    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: [ 'Gruntfile.js' ]
      },

      sass: {
        files: [
          'scss/**/*.scss'
        ],
        tasks: [ 'sass' ]
      },

      js : {
        files : [
          'js/**/*.js'
        ],
        tasks : ['concat', 'copy']
      }

    },

    copy: {
      js: {
        files: [ {
          expand: true,
          cwd: "js/libs/",
          src: [ "html5shiv.js", "modernizr.js" ],
          dest: "dist/js/"
        } ]
      },
      media : {
        files: [ {
          expand: true,
          cwd: "media/",
          src: ["**"],
          dest: "dist/media/"
        } ]
      }
    },


    concat: {
      js: {
        files: [ {
          src: [
            'js/libs/underscore-min.js',
            'js/libs/flexslider.min.js',
            'js/site.js'
          ],
          dest: 'dist/js/all.js'
        } ]
      }
    },


    clean: [ "dist" ]


  } );

  grunt.loadNpmTasks( 'grunt-sass' );
  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  grunt.loadNpmTasks( 'grunt-contrib-copy' );
  grunt.loadNpmTasks( 'grunt-contrib-clean' );
  grunt.loadNpmTasks( 'grunt-contrib-concat' );
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );

  // This task is used for local development
  grunt.registerTask( 'local', [ 'clean', 'sass:local', 'concat', 'copy' ] );

  grunt.registerTask( 'deploy', [ 'clean', 'sass:deploy', 'concat', 'copy', 'uglify' ] );


  grunt.registerTask( 'default', [ 'local' ] );
  grunt.registerTask( 'dev', [ 'local', 'watch' ] );
};
