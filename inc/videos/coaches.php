<video id="coaches-video" class="video-js vjs-default-skin ir-inner-fill" controls preload="metadata">
    <source src="/dist/media/top100/top100.mp4" type="video/mp4" />
    <source src="/dist/media/top100/top100.ogv" type="video/ogg" />
    <source src="/dist/media/top100/top100.webm" type="video/webm" />
    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
