<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/vars.php'; ?>

<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:700italic,400italic|Open+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

<![if !IE]>
	<link rel="stylesheet" media="not print" type="text/css" href="/dist/css/styles.css?v=<?=$VERSION_CSS;?>" />
	<link rel="stylesheet" media="print" type="text/css" href="/dist/css/print.css?v=<?=$VERSION_CSS;?>" />
<![endif]>
<!--[if gte IE 9]>
	<link rel="stylesheet" media="not print" type="text/css" href="/dist/css/styles.css?v=<?=$VERSION_CSS;?>" />
	<link rel="stylesheet" media="print" type="text/css" href="/dist/css/print.css?v=<?=$VERSION_CSS;?>" />
<![endif]-->
<!--[if lt IE 9]>
	<script type="text/javascript" src="/dist/js/html5shiv.js?v=<?=$VERSION_JS;?>"></script>
	<link rel="stylesheet" type="text/css" href="/dist/css/legacy-styles.css?v=<?=$VERSION_CSS;?>" />
	<link rel="stylesheet" type="text/css" href="/dist/css/legacy-print.css?v=<?=$VERSION_CSS;?>" />
<![endif]-->

<link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">