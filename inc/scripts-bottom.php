<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/vars.php'; ?>

<![if !IE]>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<![endif]>
<!--[if lte IE 9]>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<![endif]-->
<script src = "/dist/js/all.js?v=<?=$VERSION_JS;?>"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?=$GA_ID;?>', 'auto');
  ga('send', 'pageview');

</script>
<noscript>
<img src="http://nojsstats.appspot.com/<?=$GA_ID;?>/<?php echo $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?><?php if($_SERVER['HTTP_REFERER']){echo '?r='.$_SERVER['HTTP_REFERER'];}; ?>&dummy=<?php echo rand(); ?>" />
</noscript>

<script>
	window.HELP_IMPROVE_VIDEOJS = false;
</script>
<script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>