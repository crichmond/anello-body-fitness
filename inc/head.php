<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/vars.php'; ?>
<!--[if HTML5]><![endif]-->
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 ie6" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie10 lt-ie9 lt-ie8 ie7" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie10 lt-ie9 ie8" lang="en" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10 ie9" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"> <!--<![endif]-->
<head>
<!--[if !HTML5]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->

	<!-- General meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name = "keywords" content = "<?=$NAME;?>, personal trainer in parma heights, personal trainer near cleveland, trainer, training, cleveland, parma, parma heights, strongsville, fat loss, gym, fitness, health, strength, weightlifting, bodybuilding, strength, mental training, planet fitness, 24 hour fitness, fitness 19, LA fitness, bally's" />
	<meta name = "description" content = "<?=$page_description;?>" />
	<meta name = "author" content = "<?=$NAME;?>" />
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="canonical" content="<?=$URL_CANONICAL;?>" />

	<!-- don't use DMOZ listing for summary data -->
	<?php if($is404==true):?>
		<meta name="robots" content="noodp,noindex,nofollow">
	<?php else: ?>
		<meta name="robots" content="noodp">
	<?php endif; ?>

	<!-- Open Graph meta -->
	<meta property="og:title" content="<?=$page_title;?>" />
	<meta property="og:type" content="company" />
	<meta property="og:url" content="http://<?=$_SERVER['HTTP_HOST'];?>" />
	<meta property="og:image" content="http://<?=$_SERVER['HTTP_HOST'];?>/img/logo.png" />
	<meta property="og:site_name" content="<?=$BUSINESS;?>" />
	<meta property="og:description" content="<?=$page_description;?>" />

	<!-- Dublin Core meta -->
	<meta name="DC.Title" content="<?=$page_title;?>" />
	<meta name="DC.Creator" content="<?=$NAME;?>" />
	<meta name="DC.Subject" content="<?=$DESCRIPTION;?>" />
	<meta name="DC.Description" content="<?=$page_description;?>" />
	<meta name="DC.Publisher" content="<?=$NAME;?>" />
	<meta name="DC.Contributor" content="<?=$NAME;?>" />
	<meta name="DC.Date" content="2015-12-2" />
	<meta name="DC.Type" content="Text" />
	<meta name="DC.Type" content="Image" />
	<meta name="DC.Format" content="text/html" />
	<meta name="DC.Language" content="en" />
	<meta name="DC.Coverage" content="<?=$ADDRESS_STATE;?>" />
	<meta name="DC.Rights" content="Copyright <?php echo date("Y"); ?> <?=$BUSINESS;?>" />

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#580505">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#580505">

	<!-- Google, Microsoft site verification -->
	<meta name="google-site-verification" content="qMct8zGnyRzdoll2g9_UQt_vNELRdgNHfUjyucHLAtw" />
	<meta name="msvalidate.01" content="49271647893850558F6B5FBA5CB2ACE9" />

	<title><?=$page_title;?></title>
	<base href="http://<?=$_SERVER['HTTP_HOST'];?>" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="UTF-8">

	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/styles.php'; ?>

	<script>
	<?php include $_SERVER['DOCUMENT_ROOT'].'/dist/js/modernizr.js'; ?>
	</script>

</head>