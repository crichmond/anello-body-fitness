
<?php
	$VERSION_JS="1.0.2";
	$VERSION_CSS="1.0.5";

	$GA_ID="UA-8319479-8";

	$NAME="Vince Anello";
	$BUSINESS="Anello Body Fitness";
	$DESCRIPTION="Personal training by weightlifting legend Vince Anello.";

	$_URL_PREFERRED_FULL = "http://www.americanstrengthlegends.com" . $_SERVER['REQUEST_URI'];
	$_PARSED_URI = parse_url( $_URL_PREFERRED_FULL );

	$URL_CANONICAL = $_PARSED_URI["scheme"] . "://" . $_PARSED_URI["host"];

	if( $_PARSED_URI["path"] != "/" ) {
		$URL_CANONICAL .= $_PARSED_URI["path"];
	}

	$URL_STORE="https://anellobodyfitness.ecwid.com";
	$URL_WUFOO="https://vectorbridge.wufoo.com/forms/";
	$URL_YELP="http://www.yelp.com/biz/anello-body-fitness-parma";
	$URL_FACEBOOK="https://www.facebook.com/vince.anello";

	$EMAIL="anellobodyfitness@twc.com";

	$PHONE="(440) 884-1556";
	$PHONE_RAW="4408841556";

	$ADDRESS_STREET="6575 Pearl Rd #C";
	$ADDRESS_CITY="Cleveland";
	$ADDRESS_STATE="Ohio";
	$ADDRESS_POSTAL="44134";
?>