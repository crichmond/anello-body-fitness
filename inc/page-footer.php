<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/vars.php'; ?>
<footer class="footer">
	<div class="page-width">
		<div class = "row clearfix">
			<div class = "col col1 no-print">
				<nav class="footer_nav" id="footer_nav">
					<ul>
						<li><a href="http://<?=$_SERVER['HTTP_HOST'];?>/">Home</a></li>
						<li><a href="http://<?=$_SERVER['HTTP_HOST'];?>/about/">About</a></li>
						<li><a href="http://<?=$_SERVER['HTTP_HOST'];?>/mental-training/">Mental Training</a></li>
						<li><a href="http://<?=$_SERVER['HTTP_HOST'];?>/testimonials/">Testimonials</a></li>
						<li><a href="<?=$URL_STORE;?>" target="_blank">Store</a></li>
						<li class="last"><a href="http://<?=$_SERVER['HTTP_HOST'];?>/contact/">Contact</a></li>
					</ul>
				</nav>
			</div>
			<div class = "col col2">
				<section class="contact">
					<p>
						<strong>Address</strong><br />
						<?=$ADDRESS_STREET;?><br />
						<?=$ADDRESS_CITY;?>, <?=$ADDRESS_STATE;?> <?=$ADDRESS_POSTAL;?>
					</p>
					<p>
						<strong>Telephone</strong><br />
						<span class="ellipsis"><a href="tel:<?=$PHONE_RAW;?>"><?=$PHONE;?></a></span>
					</p>
					<p>
						<strong>Email</strong><br />
						<span class="ellipsis"><a title="Email Vince" href="mailto:<?=$EMAIL;?>"><?=$EMAIL;?></a></span>
					</p>
				</section>
				<section class="credits">
					<p>&copy;<?php echo date("Y"); ?> <?=$BUSINESS;?></p>
					<p>Design &amp; development by <a href="http://www.vectorbridge.com" title="Vector Bridge | Web Development and Design from Cleveland, OH" target="_blank">Vector Bridge, Inc.</a></p>
				</section>
			</div>
		</div>
	</div>
</footer>