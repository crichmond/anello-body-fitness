<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/vars.php'; ?>

<header class="header">
	<div class="page-width apply-relative">
		<section itemscope itemtype="http://schema.org/ExerciseGym" class="clearfix">
			<a href="http://<?=$_SERVER['HTTP_HOST'];?>/" title="Return to <?=$BUSINESS;?> Homepage" class="header_logo">
				<span itemprop="name" class="visuallyhidden"><?=$BUSINESS;?></span>
			</a>
			<div class = "lastCol header_contact">
				<a href = "tel:<?=$PHONE_RAW;?>" class="header_phone"><i class="fa fa-phone"></i><span itemprop="telephone"><?=$PHONE;?></span></a>
				<p itemprop="address" class="header_address" itemscope itemtype="http://schema.org/PostalAddress">
					<i class="fa fa-map-marker"></i>
					<span itemprop="streetAddress"><?=$ADDRESS_STREET;?><span class="hide-mobile">, <?=$ADDRESS_CITY;?></span></span>
					<meta itemprop="addressLocality" content="<?=$ADDRESS_CITY;?>" />
					<meta itemprop="addressRegion" content="<?=$ADDRESS_STATE;?>" />
					<meta itemprop="postalCode" content="<?=$ADDRESS_POSTAL;?>" />
				</p>

			</div>

			<meta itemprop="founder" content="<?=$NAME;?>" />
			<meta itemprop="logo" content="http://<?=$_SERVER['HTTP_HOST'];?>/img/logo.png" />
			<meta itemprop="url" content="http://<?=$_SERVER['HTTP_HOST'];?>" />
			<meta itemprop="description" content="<?=$DESCRIPTION;?>" />
			<meta itemprop="email" content="<?=$EMAIL;?>" />

		</section>

		<nav class="header_nav no-print">
			<div class="nav-l">
				<a href="http://<?=$_SERVER['HTTP_HOST'];?>/about/">About</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'];?>/mental-training/">Mental Training</a>
			</div>
			<div class="nav-r">
				<a href="http://<?=$_SERVER['HTTP_HOST'];?>/testimonials/">Testimonials</a>
				<a class="tablet-store-link" target="_blank" href="<?=$URL_STORE;?>">Store</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'];?>/contact/">Contact</a>
			</div>
			<a class="desktop-store-link" href="<?=$URL_STORE;?>" target="_blank">Check out our Store!</a>
			<a class="mobile-nav-link scrollTo" href="#footer_nav"><i class="fa fa-bars"></i>Menu</a>
			<div class="social-links">
				<a class="social social--fb" title="Vince Anello's Facebook Page" href="<?=$URL_FACEBOOK;?>" target="_blank"><i class="fa fa-facebook"></i></a>
				<a class="social social--yelp" title="Review Anello Body Fitness on Yelp" href="<?=$URL_YELP;?>" target="_blank"><i class="fa fa-yelp"></i></a>
			</div>
		</nav>
	</div>
</header>