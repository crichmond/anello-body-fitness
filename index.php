<?php
	$page_title = 'Anello Body Fitness | Personal Training By Powerlifting And Bodybuilding Legend Vince Anello';
	$page_description = 'Anello Body Fitness is the total fitness gym owned by 5-time World Champion Vince Anello. Lose weight, gain muscle, and feel great at Anello Body Fitness!';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
?>
<body id="page_home">

	<div class="header-and-body">

		<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-header.php'; ?>

		<?php
		// ----------------------------------------------------------------------------
		// BEGIN CONTENT
		// ----------------------------------------------------------------------------
		?>

		<section class="page-content">
			<h1 class = "visuallyhidden">Anello Body Fitness</h1>

			<article class="page-width">
				<div class="hero-content flex">
					<div class="hero-content__img">
						<div class="ir">
							<div class="ir-inner">
							</div>
						</div>
					</div>
					<div class="hero-content__copy">
						<h1 class="title">Affordable, personalized training <br/>from a World Champion</h1>
						<p class="copy">Vince Anello has <strong>5</strong> world championships, <strong>20</strong> world records, and a lifetime of passion for <strong>fitness education</strong>.</p>
						<p class="copy">Let him show you how to get into the best shape of your life!</p>
						<a title="Get a free consultation from Anello Body Fitness!" href="/contact/" class="btn btn--red margin-t">Get a Free Consultation!</a>
					</div>
				</div>
			</article>

			<div class="page-width">

				<div class = "row clearfix">
					<div class="col">

						<section class="mod dark">
							<div class="bg">
								<img src="/img/group.jpg" class="image-group" />
								<h1 class="h2 margin-top--large">What Vince Can Do for You</h1>
								<p>At Anello Body Fitness, Vince forms a tailor-made workout based on your fitness goals. Vince has helped multitudes of clients to lose weight, gain muscle mass, and feel good about their bodies. No two workouts with Vince are the same. He focuses on muscle confusion to get the most out of your efforts. Types of training you can expect to encounter at Anello Body Fitness are:</p>
								<ul class="text-col2 style-ul">
									<li><i class="fa fa-check"></i>Full-body stretching</li>
									<li><i class="fa fa-check"></i>Aerobic exercise</li>
									<li><i class="fa fa-check"></i>Resistance training</li>
									<li><i class="fa fa-check"></i>Aerobic weight training</li>
									<li><i class="fa fa-check"></i>Bodyweight training</li>
									<li><i class="fa fa-check"></i>Core strength exercise</li>
									<li><i class="fa fa-check"></i>Rope training</li>
									<li><i class="fa fa-check"></i>Wii fit cooldown exercise</li>
									<li><i class="fa fa-check"></i>Mental training</li>
									<li><i class="fa fa-check"></i>Diet consultation</li>
								</ul>
							</div>
						</section>
						<div class="row">
							<section class="mod col mental-training flex">
								<div class="bg">
									<h1 class="h2">
										<a title="Mental Training from Anello Body Fitness" href="/mental-training/">Mental Training&nbsp;<i class="fa fa-arrow-circle-right fa--trailing"></i></a>
									</h1>
									<p>The common denominator of all success is mental attitude. At Anello Body Fitness, Vince focuses as much on training your mind as your body.</p>
									<a title="Mental Training from Anello Body Fitness" href="/mental-training/" class="btn">Learn More</a>
								</div>
							</section>

							<section class="mod col skype-training flex">
								<div class="bg">
									<h1 class="h2">
										<a title="Contact Anello Body Fitness to find out about training with Vince Anello via Skype!" href="/contact/">Train By Skype!&nbsp;<i class="fa fa-skype fa--trailing"></i></a>
									</h1>
									<p>Vince now offers one-on-one training via Skype! Get in touch to find out how you can train with a powerlifting legend from the comfort of your own home gym.</p>
									<a title="Contact Anello Body Fitness to find out about training with Vince Anello via Skype!" href="/contact/" class="btn">Get in Touch</a>
								</div>
							</section>

							<section class="mod col testimonials flex">
								<div class="bg">
									<h1 class="h2"><a title="Testimonials From Anello Body Fitness Clients" href="/testimonials/">Testimonials&nbsp;<i class="fa fa-arrow-circle-right fa--trailing"></i></a></h1>
									<div class="flexslider">
										<ul class = "slides">
											<li>
												<blockquote>
													<p>There are a lot of other places I could go to exercise. I choose Anello Body Fitness because each session is personalized, different, and fun.</p>
													<cite>Madeline</cite>
												</blockquote>
											</li>
											<li>
												<blockquote>
													<p>I feel comfortable working with Vince because of his extensive experience, plus it's fun to think that you're training with a Legend!</p>
													<cite>Jen</cite>
												</blockquote>
											</li>
											<li>
												<blockquote>
													<p>I've always worked out, but the best results that I've gotten are from training here.</p>
													<cite>Daniela</cite>
												</blockquote>
											</li>
											<li>
												<blockquote>
													<p>You will wonder why it took you so long to figure out that weight training is imperative.. to attain the body you have always wanted!</p>
													<cite>Nicole</cite>
												</blockquote>
											</li>
											<li>
												<blockquote>
													<p>You know you've got one of the best guiding you when you're training with Vince.</p>
													<cite>Cody</cite>
												</blockquote>
											</li>

										</ul>
									</div>
								</div>
							</section>

							<section class="mod col testimonials flex">
								<div class="bg">
									<h1 class="h2">The Latest News from Vince</h1>
									<h2 class="h4">Vince was included in the Top 100 Strongest Coaches To Learn From In 2016!</h2>
									<div class="vb-cols vb-cols--flex">
										<div class="vb-col--1-1 vb-col-md--1-2 vb-col-lg--2-3">
											<p>Given Vince's incredible accomplishments and his many years teaching and training, it's no wonder he was included in the <a title="Top 100 Strongest Coaches To Learn From In 2016" href="http://anunconventionalife.com/top-100-plus-strongest-coaches-to-learn-from-in-2016-and-beyond/" target="_blank">Top 100 Strongest Coaches To Learn From In 2016!</a> There is no shortage of trainers you can work with, but only one with Vince's unique combination of experience, drive, and passion for total mind and body fitness&mdash;the man himself, Vince Anello!</p>
											<p>It's a real honor to be included on this list among so many fitness greats, so here's a brief message from Vince expressing his gratitude.</p>
											<p><a title="Get a free consultation from Anello Body Fitness!" href="/contact/" class="btn btn--red margin-t">Train with the best!</a></p>
										</div>
										<div class="vb-col--1-1 vb-col-md--1-2 vb-col-lg--1-3 ir ir--3-4 in-front--under-md margin-b">
											<div class="ir-inner">
												<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/videos/coaches.php'; ?>
											</div>
										</div>
									</div>
							</section>
						</div>

					</div>

				</div>
			</div>
		</section>

		<?php
		// ----------------------------------------------------------------------------
		// END CONTENT
		// ----------------------------------------------------------------------------
		?>
	</div>

	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-footer.php'; ?>


	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/scripts-bottom.php'; ?>
</body>
</html>
