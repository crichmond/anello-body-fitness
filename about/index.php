<?php
	$page_title = 'About Anello Body Fitness and Vince Anello | Anello Body Fitness';
	$page_description = 'There\'s more to Vince Anello than his gym and a bunch of World Championships. Find out about him here.';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
?>

    <body id="page_about">
        <div class="header-and-body">
            <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-header.php'; ?>
                <?php
		// ----------------------------------------------------------------------------
		// BEGIN CONTENT
		// ----------------------------------------------------------------------------
		?>
                    <section class="page-content">
                        <div class="page-width">
                            <h1 class="center-text--desktop">About Anello Body Fitness</h1>
                            <div class="vb-cols">
                                <section class="vb-col--1-1 mod mod--compact">
                                    <div class="bg">

                                        <h1 class="h4"><aside class="pill pill--red">Latest News</aside> Vince was included in the Top 100 Strongest Coaches To Learn From In 2016!
                                        </h1>
                                        <div id="coaches-primary">
                                            Given Vince's incredible accomplishments and his many years teaching and training, it's no wonder he was included in the <a title="Top 100 Strongest Coaches To Learn From In 2016" href="http://anunconventionalife.com/top-100-plus-strongest-coaches-to-learn-from-in-2016-and-beyond/" target="_blank">Top 100 Strongest Coaches To Learn From In 2016!</a> It's a real honor to be included on this list among so many fitness greats&nbsp;&mdash; <button class="btn btn--dark btn--inline click-toggle" data-toggle-target="#coaches-primary,#coaches-secondary"><i class="fa fa-video-camera"></i>click here</button> for a brief message from Vince expressing his gratitude.
                                        </div>
                                        <figure class="ir ir--3-4 hide" id="coaches-secondary">
                                            <div class="ir-inner">
                                                <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/videos/coaches.php'; ?>
                                            </div>
                                            <figcaption>
                                            Vince expresses his gratitude for being included in <a title="Top 100 Strongest Coaches To Learn From In 2016" href="http://anunconventionalife.com/top-100-plus-strongest-coaches-to-learn-from-in-2016-and-beyond/" target="_blank">Top 100 Strongest Coaches To Learn From In 2016!</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </section>
                                <div class="vb-col--1-1">
                                    <section class="clearfix">
                                        <figure class="img-wrap margin-b--large">
                                            <img alt="Powerlifting Legend Vince Anello with Parma Heights Mayor Mike Byrne at the ribbon-cutting ceremony for Anello Body Fitness." src="/img/anello-body-fitness-ribbon-cutting.jpg">
                                            <figcaption class="can-nest">
                                                Powerlifting Legend Vince Anello with Parma Heights Mayor Mike Byrne at the ribbon-cutting ceremony for Anello Body Fitness.
                                            </figcaption>
                                        </figure>

                                        <h1 class="h2">Anello Body Fitness</h1>
                                        <p>Anello Body Fitness was opened by powerlifting legend Vince Anello in 2000, and has been changing peoples' lives ever since.</p>
                                        <p>Vince is a five-time world champion powerlifter who holds 20 world records in deadlifting. His career has also included professional bodybuilding and physical education. All this knowledge and passion has been distilled into Anello Body Fitness.</p>
                                        <p>Vince goes one-on-one with his clients to give them the physical strength and mental fortitude necessary to achieve their goals. He knows there's more to gaining strength and getting in shape than just lifting and running&nbsp;&mdash; it takes focus, dedication, and a positive mental attitude. That's why Vince focuses on developing the mind just as much as the body.</p>
                                        <aside class="aside">
                                            <div class="aside-inner">Learn more about <a title="Mental Training by Vince Anello" href="/mental-training/">Vince's Mental Training here</a>.</div>
                                        </aside>
                                        <p>But don't think that means you won't be breaking a sweat!</p>
                                        <p>Training with Vince is intense&nbsp;&mdash; you can feel every workout getting you closer to your goals. And if you're having trouble breaking through to the next level on your own, Vince's muscle confusion techniques are sure to do the job.</p>
                                        <h2 class="h3 margin-t--large">Here are just a few of the many different ways you'll be trained at Anello Body Fitness:</h2>

                                        <ul class="text-col3 style-ul">
                                            <li><i class="fa fa-check"></i>Full-body stretching</li>
                                            <li><i class="fa fa-check"></i>Aerobic exercise</li>
                                            <li><i class="fa fa-check"></i>Resistance training</li>
                                            <li><i class="fa fa-check"></i>Aerobic weight training</li>
                                            <li><i class="fa fa-check"></i>Bodyweight training</li>
                                            <li><i class="fa fa-check"></i>Core strength exercise</li>
                                            <li><i class="fa fa-check"></i>Rope training</li>
                                            <li><i class="fa fa-check"></i>Wii fit cooldown exercise</li>
                                            <li><i class="fa fa-check"></i>Mental training</li>
                                            <li><i class="fa fa-check"></i>Diet consultation</li>
                                        </ul>
                                    </section>
                                    <hr />
                                    <section class="clearfix">
                                        <figure class="img-wrap margin-b--large">
                                            <img alt="Vince and wife Sue enjoy some time in the park with their greyhounds." src="/img/vince-and-sue.jpg">
                                            <figcaption class="can-nest">
                                                Vince and wife Sue enjoy some time in the park with their greyhounds.
                                            </figcaption>
                                        </figure>
                                        <h1 class="h2">About Vince Anello</h1>
                                        <div class="ir ir--circle flow-image flow-image--right flow-image--offset-right">
                                            <div class="ir-inner" style="background-image:url(/img/vince-lift.jpg);">
                                            </div>
                                        </div>
                                        <p>Vince Anello has been known as a powerlifting legend around the world for decades, and is considered to be one of the greatest powerlifters in history. He was the first ever to <strong>deadlift over 800 lbs</strong> at a bodyweight under 200 lbs! Vince was inducted into the Strength Hall of Fame in York, Pennsylvania in 1998, and the AAU World's Strength Sports Hall of Fame in 2015. He holds 5 world championship titles in addition to 20 world records in deadlifting, and was included in the <a title="Top 100 Strongest Coaches To Learn From In 2016" href="http://anunconventionalife.com/top-100-plus-strongest-coaches-to-learn-from-in-2016-and-beyond/" target="_blank">Top 100 Strongest Coaches To Learn From In 2016</a>.</p>
                                        <div class="ir ir--circle flow-image flow-image--left flow-image--offset-left">
                                            <div class="ir-inner" style="background-image:url(/img/vince-and-dog2.jpg);">
                                            </div>
                                        </div>
                                        <p>Vince is a proponent of physical activity for all age groups. He taught physical education classes in the Cleveland area for years. He is also an avid greyhound lover and has rescued six retired racing greyhounds to date.</p>
                                        <p>Although training with a powerlifting legend may sound intimidating, Vince is anything but that! He really gets to know his clients and their specific needs. His many years of experience in the physical health field have molded his approach to physical fitness.</p>
                                        <h2 class="margin-t--large">Vince Today</h2>
                                        <p>Although Vince spends a lot of time training his clients at Anello Body Fitness, he also likes to spend time with his wife, Sue, and their five rescued greyhounds.</p>
                                        <p>Vince has been training people since 1968, and still takes every opportunity to share his knowledge with those around him. <a title="Contact Anello Body Fitness" href="/contact/">Find out for yourself</a> what you can learn from a real American Strength Legend!</p>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
		// ----------------------------------------------------------------------------
		// END CONTENT
		// ----------------------------------------------------------------------------
		?>
        </div>
        <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-footer.php'; ?>
            <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/scripts-bottom.php'; ?>
    </body>

    </html>
