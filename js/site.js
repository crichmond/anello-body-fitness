( function( $ ) {
  'use strict';


  // -------------------------------------------------------------------
  // Zepto smooth scroll
  // https://gist.github.com/austinpray/5994652
  // -------------------------------------------------------------------
  var scrollToTimerCache;
  function smoothScroll(el, to, duration) {
    if (duration < 0) {
        return;
    }
    var el = el || $( window );
    var difference = to - el.scrollTop();
    var perTick = difference / duration * 10;
    clearTimeout( scrollToTimerCache );
    scrollToTimerCache = setTimeout(function() {
      if (!isNaN(parseInt(perTick, 10))) {
        window.scrollTo(0, $(window).scrollTop() + perTick);
        smoothScroll(el, to, duration - 10);
      }
    }.bind(this), 10);
  }




  // ---------------------------------------------------------------------------
  // Flexslider
  // ---------------------------------------------------------------------------

  function initTestimonialSlider() {
    $( ".flexslider" ).flexslider({
      animation: "slide"
    });
  }



  $( document ).ready( function() {
    // --------------------------------------------------------------------
    // Execute
    // --------------------------------------------------------------------


    // --------------------------------------------------------------------
    // Binding
    // --------------------------------------------------------------------

    $(".scrollTo")
      .on( "click", function(e) {
        e.preventDefault();
        var $this = $(e.currentTarget);
        smoothScroll($(window), $($this.attr('href')).offset().top, 200);
        setTimeout( function() {
          $this.blur();
        }, 0 );
      });

    $( ".click-toggle" )
      .on( "click", function( e ) {
        var $this = $( this ),
          target = $this.data( "toggleTarget" ),
          $target, $vis;

        if( target ) {
          $target = $( target );

          if( $target.length ) {
            e.preventDefault();
            $target.toggle();

            $vis = $target.filter( ":visible" ).eq( 0 );

            if( $vis.length ) {
              smoothScroll($(window), $vis.offset().top, 200);
            }
          }
        }
      } );

  } );

  $( window ).load( function() {
    // --------------------------------------------------------------------
    // Execute
    // --------------------------------------------------------------------

    initTestimonialSlider();
  } );


} )( jQuery );