<?php
	$page_title = 'Mental Training | Anello Body Fitness';
	$page_description = 'At Anello Body Fitness, mental health is as important as physical fitness. Vince offers training for everything from goals to relaxation.';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
?>
<body id="page_mental-training">

	<div class="header-and-body">

		<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-header.php'; ?>

		<?php
		// ----------------------------------------------------------------------------
		// BEGIN CONTENT
		// ----------------------------------------------------------------------------
		?>

		<section class="page-content">

			<div class="page-width">

				<h1 class = "visuallyhidden">Mental Training</h1>
				<h2 class = "h1 center-text--desktop">Your Mind Is Your Strongest Muscle</h2>

				<div class = "row clearfix">
					<div class="col">

						<section class = "">

							<div class="split-form">
								<header class="split-form_beside">
									<h1 class="h4">The common denominator of all success is mental attitude.</h1>
									<hr />
									<p>Vince Anello holds five World Titles and 20 deadlifting world records. He knows from experience that the right mental attitude is essential to safely push yourself toward your goals, and that's why he focuses on training the mind as well as the body at Anello Body Fitness.</p>
									<p>But Vince doesn't offer mental training only at the gym! Use this form to get in touch with Vince, and together you can develop a mental training program made specifically for <em>your</em> goals that you can use in the comfort of your own home. Vince will send you the personalized audio mental training session to focus your mind and achieve the results you want!</p>
								</header>

								<div id="wufoo-mb4lsrj1rh9tct" class="split-form_form">
									<p>Fill out your Mental Training Request Form <a title="Link to Mental Training Form for Anello Body Fitness, powered by Wufoo" href="<?=$URL_WUFOO;?>mb4lsrj1rh9tct">here</a>.</p>
								</div>
							</div>
						</section>

					</div>

				</div>
			</div>
		</section>

		<?php
		// ----------------------------------------------------------------------------
		// END CONTENT
		// ----------------------------------------------------------------------------
		?>
	</div>

	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-footer.php'; ?>


	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/scripts-bottom.php'; ?>

	<script type="text/javascript">var mb4lsrj1rh9tct;(function(d, t) {
	var s = d.createElement(t), options = {
	'userName':'vectorbridge',
	'formHash':'mb4lsrj1rh9tct',
	'autoResize':true,
	'height':'400',
	'async':true,
	'host':'wufoo.com',
	'header':'hide',
	'ssl':true};
	s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';
	s.onload = s.onreadystatechange = function() {
	var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
	try { mb4lsrj1rh9tct = new WufooForm();mb4lsrj1rh9tct.initialize(options);mb4lsrj1rh9tct.display(); } catch (e) {}};
	var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
	})(document, 'script');</script>
</body>
</html>
