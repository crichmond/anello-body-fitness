<?php
	$page_title = 'Testimonials | Anello Body Fitness';
	$page_description = 'What are people saying about Anello Body Fitness? Hear it straight from his clients.';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
?>
<body id="page_testimonials">

	<div class="header-and-body">

		<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-header.php'; ?>

		<?php
		// ----------------------------------------------------------------------------
		// BEGIN CONTENT
		// ----------------------------------------------------------------------------
		?>

		<section class="page-content">

			<div class="page-width">

				<h1 class = "center-text--desktop">Testimonials</h1>

				<div class = "row clearfix">
					<div class="col">

						<section class = "">

							<h1 class="h4 center-text--desktop">What do Vince's clients have to say about training at Anello Body Fitness?</h1>

							<blockquote id="cody">
								<p>I usually train in person with Vince weekly, but recently I thought I was going to have to miss a week due to scheduling conflicts. Missing a week can be pretty detrimental for me because I struggle with self-motivation, so Vince suggested I try training with him via <a href="http://<?=$_SERVER['HTTP_HOST'];?>/contact/"><strong>Skype</strong></a>. It worked out great! Vince talked me through variations on exercises that I could do with the free weights and resistance bands I had at home, and was still able to provide the motivation and watchful eye I depend on each week. I just set my laptop up so he could see me via the webcam and it was off to the races. Thanks, Vince!</p>
								<cite>Cody</cite>
							</blockquote>

							<blockquote id="madeline">
								<p>I started attending Anello Body Fitness in 2002 and lost 25 pounds.  In May, 2003 I was not able to attend weekly exercise sessions due to an accident. When my doctor told me I could slowly start weekly exercise sessions again, Vince helped me regain strength and flexibility.</p>
								<p>Each week I drive 20 miles to attend Anello Body Fitness in Parma Heights. There are a lot of other places I could go to exercise. I choose Anello Body Fitness because each session is personalized, different, and fun... It will take time for me to reach all of my goals but I am well on my way. <strong>Thank you Vince!</strong></p>
								<cite>Madeline</cite>
							</blockquote>

							<blockquote id="jen">
								<p>One of the reasons that Vince is an amazing trainer is because he connects with you on a personal level. He is genuinely interested in your goals and he motivates you to succeed based on those goals. Also, his workouts are never boring because they are constantly changing to get the most out of your time and effort. I feel comfortable working with him because of his extensive experience, plus it's fun to think that you're training with a Legend!</p>
								<cite>Jen</cite>
							</blockquote>

							<blockquote id="daniela">
								<p>I started training at Anello Body Fitness in November 2005. I don't have any regrets! Vince Anello is a great trainer. He makes you want to keep going and not give up. The sessions are great because each one is different every time, so you definitely won't get bored or lose interest... I've noticed a huge difference in my body from when I started with Vince till now. I've always worked out, but the best results that I've gotten are from training here. I feel in better shape overall, I've lost weight and I'm seeing definition in my arms, thighs, calf muscles and abs!!!... I would 100% recommend Vince Anello to be anyone's personal trainer, it will change your life for the better and the best part is that you'll look and feel the best you've ever felt. That's the biggest reward of all!!!</p>
								<cite>Daniela</cite>
							</blockquote>

							<blockquote id="nicole">
								<p>I have been training w/ Vince Anello since 2004 and am so pleased with the transformation that has taken place! Vince has turned me on to working out with weights and taught me how building muscle is key to healthy long term weight management. It is amazing how weights can totally change one's body in addition to the strength and confidence that you gain. I would have never thought weights would slim and trim and tone like they do, I was always hesitant to use weights fearing I would "bulk" up. Not true when you use Vince's various individually based programs and expertise on the different ways to train the body. You will wonder why it took you so long to figure out that weight training is imperative in addition to proper diet to attain the body you have always wanted! I am a believer and will always include weight training in my repertoire... <strong>thanks Vince for opening my eyes to the world of weight training!</strong></p>
								<cite>Nicole</cite>
							</blockquote>

							<blockquote id="tammy">
								<p>Vince has been my personal trainer for three years now and I have to say he rocks. After I had my second child I could not lose any weight. I have exercised all my life, mostly cardio, never weight training. I did not know anything about weights. I just thought it would make me look like a man and was not interested. That has all changed. One of my friends introduced me to Vince and I could not believe the transformation she had.  I thought I have to give him shot of course I was desperate at 5'3" and 152 pounds not good! With proper diet and weight training I now weigh 120 lbs. Vince's workouts are never the same which is called muscle confusion, that's what you need to get that lean mean ripped definition.</p>
								<cite>Tammy</cite>
							</blockquote>


						</section>

					</div>

				</div>
			</div>
		</section>

		<?php
		// ----------------------------------------------------------------------------
		// END CONTENT
		// ----------------------------------------------------------------------------
		?>
	</div>

	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-footer.php'; ?>


	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/scripts-bottom.php'; ?>
</body>
</html>
