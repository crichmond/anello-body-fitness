<?php
	$page_title = 'Page Not Found | Anello Body Fitness';
	$page_description = 'The page you\'re looking for doesn\'t exist.';
    $is404 = true;
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
?>

    <body id="page_404">
        <div class="header-and-body">
            <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-header.php'; ?>
                <?php
		// ----------------------------------------------------------------------------
		// BEGIN CONTENT
		// ----------------------------------------------------------------------------
		?>
                    <section class="page-content">
                        <div class="page-width">
                            <h1 class="center-text--desktop">Page Not Found</h1>
                            <div class="row clearfix">
                                <div class="col">
                                    <p>Sorry, but we can't find the page you're looking for.</p>
                                    <hr />
                                    <div class="row">
                                        <section class="mod col col1 flex">
                                            <div class="bg">
                                                <h1 class="h2">Interested In Training?</h2>
                                                <p>Are you interested in <strong>training at Anello Body Fitness</strong>? Or maybe <strong>training with Vince via Skype</strong> is more your thing? Then head over to our <strong><a href="<?=$_SERVER['HTTP_HOST'];?>/contact/" title="Contact Anello Body Fitness">Contact Page</a></strong> to get your <strong>free consultation</strong>!</p>
                                            </div>
                                        </section>
                                        <section class="mod col col2 flex">
                                            <div class="bg">
                                                <h1 class="h2">Get Your Head In The Game</h2>
                                                <p>Would you rather focus on setting and achieving attainable goals? Need some help getting into a calm, collected state of mind? <strong><a href="<?=$_SERVER['HTTP_HOST'];?>/mental-training/" title="Mental Training from Vince Anello">Mental Training</a> from Vince Anello</strong> can help!</p>
                                            </div>
                                        </section>
                                    </div>
                                    <section class="pad-t">
                                        <h1 class="h4">You Might Also Be Interested In...</h2>
                                        <ul class="style-ul">
                                            <li><i class="fa fa-comment"></i><a href="<?=$_SERVER['HTTP_HOST'];?>/testimonials/" title="Reviews by real Anello Body Fitness clients!">Testimonials from Anello Body Fitness clients</a></li>
                                            <li><i class="fa fa-shopping-cart"></i><a href="<?=$URL_STORE;?>" title="Anello Body Fitness Store">Our Store</a></li>
                                            <li><i class="fa fa-thumbs-up"></i><a href="<?=$_SERVER['HTTP_HOST'];?>/about/" title="About Vince Anello">Get to know five-time World Champion Vince Anello</a></li>
                                        </ul>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
		// ----------------------------------------------------------------------------
		// END CONTENT
		// ----------------------------------------------------------------------------
		?>
        </div>
        <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-footer.php'; ?>
            <?php include $_SERVER['DOCUMENT_ROOT'].'/inc/scripts-bottom.php'; ?>
    </body>

    </html>
