<?php
	$page_title = 'Contact Vince | Anello Body Fitness';
	$page_description = 'Address for Anello Body Fitness, and contact information for Vince Anello.';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/vars.php';
	include $_SERVER['DOCUMENT_ROOT'].'/inc/head.php';
?>
<body id="page_contact">

	<div class="header-and-body">

		<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-header.php'; ?>

		<?php
		// ----------------------------------------------------------------------------
		// BEGIN CONTENT
		// ----------------------------------------------------------------------------
		?>

		<section class="page-content">

			<div class="page-width">

				<h1 class = "center-text--desktop">Contact Vince</h1>

				<div class = "row clearfix">
					<div class="col">

						<section class = "">

							<div class="split-form">
								<header class="split-form_beside">
									<h1 class="h4">Call, email, or use the form on this page to get your <span class="shadow-pop little-big-text">free consultation</span> from Anello Body Fitness!</h1>
									<p><span class="pill pill--red">New!</span> Vince now offers <a title="Sign up for a Skype account" href="http://www.skype.com/en/" target="_blank">Skype</a> training! Now you can be trained by a World Champion without ever leaving your house! Be sure to include your Skype username so you can start training better today!</p>
									<hr />
									<p><?=$ADDRESS_STREET;?><br />
									<?=$ADDRESS_CITY;?>, <?=$ADDRESS_STATE;?> <?=$ADDRESS_POSTAL;?></p>
									<p class="ellipsis"><a title="Call Anello Body Fitness" href="tel:<?=$PHONE_RAW;?>"><i class="fa fa-phone"></i><?=$PHONE;?></a></p>
									<p class="ellipsis"><a title="Email Anello Body Fitness" href="mailto:<?=$EMAIL;?>"><i class="fa fa-envelope"></i><?=$EMAIL;?></a></p>
								</header>

								<div id="wufoo-zdzm8na1ehrvq4" class="split-form_form">
									<p>Get in touch with Vince Anello at Anello Body Fitness <a title="Link to Contact Form for Anello Body Fitness, powered by Wufoo" href="<?=$URL_WUFOO;?>zdzm8na1ehrvq4">here</a>.</p>
								</div>


							</div>
						</section>

					</div>

				</div>
			</div>
		</section>

		<?php
		// ----------------------------------------------------------------------------
		// END CONTENT
		// ----------------------------------------------------------------------------
		?>
	</div>

	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/page-footer.php'; ?>

	<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/scripts-bottom.php'; ?>

	<script type="text/javascript">var zdzm8na1ehrvq4;(function(d, t) {
	var s = d.createElement(t), options = {
	'userName':'vectorbridge',
	'formHash':'zdzm8na1ehrvq4',
	'autoResize':true,
	'height':'400',
	'async':true,
	'host':'wufoo.com',
	'header':'hide',
	'ssl':true};
	s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';
	s.onload = s.onreadystatechange = function() {
	var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
	try { zdzm8na1ehrvq4 = new WufooForm();zdzm8na1ehrvq4.initialize(options);zdzm8na1ehrvq4.display(); } catch (e) {}};
	var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
	})(document, 'script');</script>
</body>
</html>
