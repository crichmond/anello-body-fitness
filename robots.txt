# Allow crawling of all content
User-agent: *
Disallow: /scss/
Disallow: /js/
Disallow: /dist/
Disallow: /fonts/
Disallow: /inc/
Disallow: 404.php
Disallow: *.js
Disallow: bower.json
Disallow: package.json
Disallow: .htaccess
Disallow: .gitignore